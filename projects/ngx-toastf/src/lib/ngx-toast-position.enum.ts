export enum NgToastPosition {
    BottomCenter = 0,
    BottomFullWidth = 1,
    BottomLeft = 2,
    BottomRight = 3,
    TopCenter = 4,
    TopFullWidth = 5,
    TopLeft = 6,
    TopRight = 7,
    Center = 8
}
