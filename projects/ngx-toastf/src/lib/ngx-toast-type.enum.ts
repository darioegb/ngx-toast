export enum NgToastType {
    Success = 0,
    Info = 1,
    Warning = 2,
    Error = 3
}
